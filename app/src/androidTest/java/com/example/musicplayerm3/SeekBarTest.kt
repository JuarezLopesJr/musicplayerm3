package com.example.musicplayerm3

import androidx.compose.ui.semantics.ProgressBarRangeInfo
import androidx.compose.ui.test.assertRangeInfoEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performTouchInput
import androidx.compose.ui.test.swipeLeft
import com.example.musicplayerm3.ui.screens.player.PlayerSeekBar
import com.example.musicplayerm3.utils.Tags.TAG_LINEAR_BAR
import com.example.musicplayerm3.utils.Tags.TAG_SEEK_BAR
import com.example.musicplayerm3.utils.TestDataFactory
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.atLeastOnce
import org.mockito.kotlin.mock
import org.mockito.kotlin.never
import org.mockito.kotlin.verify

class SeekBarTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    private val nowPlaying = TestDataFactory.makeNowPlaying()

    @Test
    fun assert_Track_Progress_Displayed_When_Can_Seek() {
        composeTestRule.setContent {
            PlayerSeekBar(nowPlaying = nowPlaying, canSeekTrack = true)
        }

        composeTestRule.onNodeWithTag(TAG_SEEK_BAR)
            .assertRangeInfoEquals(
                ProgressBarRangeInfo(
                    current = nowPlaying.position.toFloat() / nowPlaying.track.length.toFloat(),
                    range = 0f..1f,
                    steps = 0
                )
            )
    }

    @Test
    fun assert_Linear_Progress_Displayed_When_Cannot_Seek() {
        composeTestRule.setContent {
            PlayerSeekBar(nowPlaying = nowPlaying, canSeekTrack = false)
        }

        composeTestRule.onNodeWithTag(TAG_LINEAR_BAR)
            .assertRangeInfoEquals(
                ProgressBarRangeInfo(
                    current = nowPlaying.position.toFloat() / nowPlaying.track.length.toFloat(),
                    range = 0f..1f,
                    steps = 0
                )
            )
    }

    @Test
    fun assert_Track_Progress_Updated_When_Can_Seek() {
        val onSeekChanged: (Float) -> Unit = mock()

        composeTestRule.setContent {
            PlayerSeekBar(
                nowPlaying = nowPlaying,
                canSeekTrack = true,
                onSeekChanged = onSeekChanged
            )
        }

        composeTestRule.onNodeWithTag(TAG_SEEK_BAR)
            .performTouchInput { swipeLeft() }

        verify(onSeekChanged, atLeastOnce()).invoke(any())
    }

    @Test
    fun assert_Track_Progress_Not_Updated_When_Cannot_Seek() {
        val onSeekChanged: (Float) -> Unit = mock()

        composeTestRule.setContent {
            PlayerSeekBar(
                nowPlaying = nowPlaying,
                canSeekTrack = false,
                onSeekChanged = onSeekChanged
            )
        }

        composeTestRule.onNodeWithTag(TAG_LINEAR_BAR)
            .performTouchInput { swipeLeft() }

        verify(onSeekChanged, never()).invoke(any())
    }
}