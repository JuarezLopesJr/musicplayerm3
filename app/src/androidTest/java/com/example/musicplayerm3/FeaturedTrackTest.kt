package com.example.musicplayerm3

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import com.example.musicplayerm3.utils.FeaturedTrack
import com.example.musicplayerm3.utils.TestDataFactory
import org.junit.Rule
import org.junit.Test

class FeaturedTrackTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Title_Displayed() {
        val track = TestDataFactory.makeTrack()
        composeTestRule.setContent {
            FeaturedTrack(track = track)
        }

        composeTestRule.onNodeWithText(track.title).assertIsDisplayed()
    }
}