package com.example.musicplayerm3

import androidx.compose.ui.test.assert
import androidx.compose.ui.test.hasTestTag
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onChildAt
import androidx.compose.ui.test.onNodeWithTag
import com.example.musicplayerm3.utils.FeaturedTracksGrid
import com.example.musicplayerm3.utils.Tags.TAG_FEATURED_TRACKS
import com.example.musicplayerm3.utils.Tags.TAG_TRACK
import com.example.musicplayerm3.utils.TestDataFactory
import org.junit.Rule
import org.junit.Test

class FeaturedTracksGridTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Featured_Tracks_Not_Displayed_When_Empty() {
        composeTestRule.setContent {
            FeaturedTracksGrid(tracks = emptyList(), onTrackClicked = {})
        }

        composeTestRule.onNodeWithTag(TAG_FEATURED_TRACKS).assertDoesNotExist()
    }

    @Test
    fun assert_Featured_Tracks_Not_Displayed_When_Null() {
        composeTestRule.setContent {
            FeaturedTracksGrid(tracks = null, onTrackClicked = {})
        }

        composeTestRule.onNodeWithTag(TAG_FEATURED_TRACKS).assertDoesNotExist()
    }

    @Test
    fun assert_Featured_Tracks_Displayed() {
        val tracks = TestDataFactory.makeContentList(6)

        composeTestRule.setContent { FeaturedTracksGrid(tracks = tracks, onTrackClicked = {}) }

        tracks.forEachIndexed { index, track ->
            composeTestRule.onNodeWithTag(TAG_FEATURED_TRACKS)
                .onChildAt(index)
                .assert(hasTestTag(TAG_TRACK + track.id))
        }
    }
}