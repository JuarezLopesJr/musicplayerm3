package com.example.musicplayerm3

import androidx.compose.ui.test.assert
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.hasTestTag
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onChildAt
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performScrollTo
import androidx.test.platform.app.InstrumentationRegistry
import com.example.musicplayerm3.ui.screens.search.SearchResults
import com.example.musicplayerm3.utils.Tags.TAG_SEARCH_RESULTS
import com.example.musicplayerm3.utils.Tags.TAG_TRACK
import com.example.musicplayerm3.utils.TestDataFactory
import org.junit.Rule
import org.junit.Test

class SearchResultsTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    private val tracks = TestDataFactory.makeContentList(6)

    private fun getTestString(id: Int) =
        InstrumentationRegistry.getInstrumentation().targetContext.getString(id)

    @Test
    fun assert_Empty_State_Displayed() {
        composeTestRule.setContent {
            SearchResults(results = emptyList(), onTrackClicked = {})
        }

        composeTestRule.onNodeWithText(getTestString(R.string.message_search_results))
            .assertIsDisplayed()
    }

    @Test
    fun assert_Empty_State_Not_Displayed_When_Results_Exist() {
        composeTestRule.setContent {
            SearchResults(
                results = tracks,
                onTrackClicked = {}
            )
        }

        composeTestRule.onNodeWithText(getTestString(R.string.message_search_results))
            .assertDoesNotExist()
    }

    @Test
    fun assert_Search_Results_Displayed() {
        composeTestRule.setContent {
            SearchResults(
                results = tracks,
                onTrackClicked = {}
            )
        }

        tracks.forEachIndexed { index, track ->
            composeTestRule.onNodeWithTag(TAG_SEARCH_RESULTS)
                .onChildAt(index).apply {
                    performScrollTo()
                    assert(hasTestTag(TAG_TRACK + track.id))
                }
        }
    }
}