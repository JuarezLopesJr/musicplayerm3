package com.example.musicplayerm3

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.hasTestTag
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performScrollToNode
import com.example.musicplayerm3.data.Track
import com.example.musicplayerm3.utils.NewTracksRow
import com.example.musicplayerm3.utils.Tags.TAG_NEW_TRACKS
import com.example.musicplayerm3.utils.Tags.TAG_TRACK
import com.example.musicplayerm3.utils.TestDataFactory
import dev.chrisbanes.snapper.ExperimentalSnapperApi
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

@ExperimentalSnapperApi class NewTrackRowTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    private val tracks = TestDataFactory.makeContentList(6)

    @Test
    fun assert_New_Tracks_Not_Displayed_When_Empty() {
        composeTestRule.setContent {
            NewTracksRow(tracks = emptyList(), onTrackClicked = {})
        }

        composeTestRule.onNodeWithTag(TAG_NEW_TRACKS).assertDoesNotExist()
    }

    @Test
    fun assert_New_Tracks_Not_Displayed_When_Null() {
        composeTestRule.setContent {
            NewTracksRow(tracks = null, onTrackClicked = {})
        }

        composeTestRule.onNodeWithTag(TAG_NEW_TRACKS).assertDoesNotExist()
    }

    @Test
    fun assert_New_Tracks_Displayed() {
        composeTestRule.setContent { NewTracksRow(tracks = tracks, onTrackClicked = {}) }

        tracks.forEach { track ->
            composeTestRule.onNodeWithTag(TAG_NEW_TRACKS)
                .performScrollToNode(hasTestTag(TAG_TRACK + track.id))
                .assertIsDisplayed()
        }
    }

    @Test
    fun assert_Selecting_New_Track_Triggers_Listener() {
        val onTrackClicked: (Track) -> Unit = mock()

        composeTestRule.setContent {
            NewTracksRow(tracks = tracks, onTrackClicked = onTrackClicked)
        }

        composeTestRule.onNodeWithTag(TAG_TRACK + tracks[2].id)
            .performClick()

        verify(onTrackClicked).invoke(tracks[2])
    }
}