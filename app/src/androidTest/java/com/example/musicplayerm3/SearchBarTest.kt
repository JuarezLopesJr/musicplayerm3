package com.example.musicplayerm3

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performTextInput
import androidx.test.platform.app.InstrumentationRegistry
import com.example.musicplayerm3.ui.screens.search.SearchBar
import com.example.musicplayerm3.utils.Tags.TAG_SEARCH_BAR
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalMaterial3Api class SearchBarTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    private fun getTestString(id: Int) =
        InstrumentationRegistry.getInstrumentation().targetContext.getString(id)

    @Before
    fun setComposeTestRule() {
        composeTestRule.setContent {
            SearchBar(query = null, handleQuery = {}, clearQuery = {})
        }
    }

    @Test
    fun assert_Hint_Displayed_By_Default() {
        composeTestRule.onNodeWithText(getTestString(R.string.hint_search)).assertIsDisplayed()
    }

    @Test
    fun assert_Hint_Hidden_When_Focused() {
        composeTestRule.onNodeWithTag(TAG_SEARCH_BAR)
            .performClick()

        composeTestRule.onNodeWithText(getTestString(R.string.hint_search))
            .assertDoesNotExist()
    }

    @Test
    fun assert_Search_Icon_Displayed_By_Default() {
        composeTestRule.onNodeWithContentDescription(getTestString(R.string.cd_search))
            .assertIsDisplayed()
    }

    @Test
    fun assert_Clear_Icon_Displayed_When_Query_Entered() {
        composeTestRule.onNodeWithTag(TAG_SEARCH_BAR)
            .performTextInput("query")

        composeTestRule.onNodeWithContentDescription(getTestString(R.string.cd_clear_query))
            .assertIsDisplayed()
    }
}