package com.example.musicplayerm3

import androidx.compose.material.BackdropValue
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.rememberBackdropScaffoldState
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import com.example.musicplayerm3.data.MusicDashboardState
import com.example.musicplayerm3.ui.screens.player.MusicPlayer
import com.example.musicplayerm3.utils.Tags.TAG_PLAYER
import com.example.musicplayerm3.utils.Tags.TAG_PLAYER_BAR
import com.example.musicplayerm3.utils.TestDataFactory
import org.junit.Rule
import org.junit.Test

@ExperimentalMaterialApi class MusicPlayerTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Collapsed_Player_Bar_Displayed() {
        composeTestRule.setContent {
            val scaffoldState = rememberBackdropScaffoldState(
                initialValue = BackdropValue.Revealed
            )

            MusicPlayer(
                state = MusicDashboardState(nowPlaying = TestDataFactory.makeNowPlaying()),
                scaffoldState = scaffoldState,
                handleEvent = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_PLAYER_BAR)
            .assertIsDisplayed()
    }

    @Test
    fun assert_Player_Never_Revealed_While_Player_Bar_Collapsed() {
        composeTestRule.setContent {
            val scaffoldState = rememberBackdropScaffoldState(
                initialValue = BackdropValue.Revealed
            )

            MusicPlayer(
                state = MusicDashboardState(nowPlaying = TestDataFactory.makeNowPlaying()),
                scaffoldState = scaffoldState,
                handleEvent = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_PLAYER)
            .assertDoesNotExist()
    }

    @Test
    fun assert_Revealed_Player_Bar_Displayed() {
        composeTestRule.setContent {
            val scaffoldState = rememberBackdropScaffoldState(
                initialValue = BackdropValue.Concealed
            )

            MusicPlayer(
                state = MusicDashboardState(nowPlaying = TestDataFactory.makeNowPlaying()),
                scaffoldState = scaffoldState,
                handleEvent = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_PLAYER)
            .assertIsDisplayed()
    }

    @Test
    fun assert_Player_Bar_Never_Displayed_When_Player_Revealed() {
        composeTestRule.setContent {
            val scaffoldState = rememberBackdropScaffoldState(
                initialValue = BackdropValue.Concealed
            )

            MusicPlayer(
                state = MusicDashboardState(nowPlaying = TestDataFactory.makeNowPlaying()),
                scaffoldState = scaffoldState,
                handleEvent = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_PLAYER_BAR)
            .assertDoesNotExist()
    }
}