package com.example.musicplayerm3

import androidx.compose.ui.test.assertContentDescriptionEquals
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.test.platform.app.InstrumentationRegistry
import com.example.musicplayerm3.data.NowPlayingState
import com.example.musicplayerm3.ui.screens.player.PlayerBar
import com.example.musicplayerm3.utils.Tags.TAG_PLAY_PAUSE
import com.example.musicplayerm3.utils.Tags.TAG_TRACK_ARTIST
import com.example.musicplayerm3.utils.Tags.TAG_TRACK_TITLE
import com.example.musicplayerm3.utils.TestDataFactory
import org.junit.Rule
import org.junit.Test

class PlayerBarTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    private val track = TestDataFactory.makeTrack()
    private val nowPlaying = TestDataFactory.makeNowPlaying(track = track)

    private fun getTestString(id: Int) =
        InstrumentationRegistry.getInstrumentation().targetContext.getString(id)

    @Test
    fun assert_Track_Title_Displayed() {
        composeTestRule.setContent {
            PlayerBar(nowPlaying = nowPlaying, toggleNowPlayingState = {})
        }

        composeTestRule.onNodeWithTag(TAG_TRACK_TITLE, useUnmergedTree = true)
            .assertTextEquals(track.title)

    }

    @Test
    fun assert_Track_Artist_Displayed() {
        composeTestRule.setContent {
            PlayerBar(nowPlaying = nowPlaying, toggleNowPlayingState = {})
        }

        composeTestRule.onNodeWithTag(TAG_TRACK_ARTIST, useUnmergedTree = true)
            .assertTextEquals(track.artist)

    }

    @Test
    fun assert_Playing_State_Displayed() {
        composeTestRule.setContent {
            PlayerBar(
                nowPlaying = TestDataFactory.makeNowPlaying(
                    track = track,
                    state = NowPlayingState.PLAYING
                ),
                toggleNowPlayingState = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_PLAY_PAUSE)
            .assertContentDescriptionEquals(getTestString(R.string.cd_pause))
    }

    @Test
    fun assert_Pause_State_Displayed() {
        composeTestRule.setContent {
            PlayerBar(
                nowPlaying = TestDataFactory.makeNowPlaying(
                    track = track,
                    state = NowPlayingState.PAUSED
                ),
                toggleNowPlayingState = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_PLAY_PAUSE)
            .assertContentDescriptionEquals(getTestString(R.string.cd_play))
    }
}