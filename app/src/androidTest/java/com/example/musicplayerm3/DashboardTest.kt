package com.example.musicplayerm3

import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import com.example.musicplayerm3.data.MusicDashboardState
import com.example.musicplayerm3.ui.screens.dashboard.Dashboard
import com.example.musicplayerm3.utils.Tags.TAG_DASHBOARD
import com.example.musicplayerm3.utils.Tags.TAG_DISMISS_PLAYER
import com.example.musicplayerm3.utils.Tags.TAG_PLAYER
import com.example.musicplayerm3.utils.Tags.TAG_PLAYER_BAR
import com.example.musicplayerm3.utils.Tags.TAG_SEARCH_BAR
import com.example.musicplayerm3.utils.TestDataFactory
import dev.chrisbanes.snapper.ExperimentalSnapperApi
import org.junit.Rule
import org.junit.Test

@ExperimentalMaterialApi
@ExperimentalMaterial3Api
@ExperimentalSnapperApi class DashboardTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Content_Area_Displayed() {
        composeTestRule.setContent { Dashboard(state = MusicDashboardState(), handleEvent = {}) }
        composeTestRule.onNodeWithTag(TAG_DASHBOARD).assertIsDisplayed()
    }

    @Test
    fun assert_Search_Bar_Displayed() {
        composeTestRule.setContent { Dashboard(state = MusicDashboardState(), handleEvent = {}) }
        composeTestRule.onNodeWithTag(TAG_SEARCH_BAR).assertIsDisplayed()
    }

    @Test
    fun assert_Full_Screen_Player_Revealed() {
        composeTestRule.setContent {
            Dashboard(
                state = MusicDashboardState(
                    nowPlaying = TestDataFactory.makeNowPlaying()
                ),
                handleEvent = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_PLAYER_BAR).performClick()
        composeTestRule.onNodeWithTag(TAG_PLAYER).assertIsDisplayed()
    }

    @Test
    fun assert_Player_Dismissed_Hides_Player() {
        composeTestRule.setContent {
            Dashboard(
                state = MusicDashboardState(
                    nowPlaying = TestDataFactory.makeNowPlaying()
                ),
                handleEvent = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_PLAYER_BAR).performClick()
        composeTestRule.onNodeWithTag(TAG_DISMISS_PLAYER).performClick()
        composeTestRule.onNodeWithTag(TAG_PLAYER).assertDoesNotExist()
    }

    @Test
    fun assert_Player_Dismissed_Shows_Player_Bar() {
        composeTestRule.setContent {
            Dashboard(
                state = MusicDashboardState(
                    nowPlaying = TestDataFactory.makeNowPlaying()
                ),
                handleEvent = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_PLAYER_BAR).performClick()
        composeTestRule.onNodeWithTag(TAG_DISMISS_PLAYER).performClick()
        composeTestRule.onNodeWithTag(TAG_PLAYER_BAR).assertIsDisplayed()
    }
}