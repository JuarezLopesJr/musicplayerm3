package com.example.musicplayerm3

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import com.example.musicplayerm3.utils.NewTracks
import com.example.musicplayerm3.utils.TestDataFactory
import org.junit.Rule
import org.junit.Test

class NewTrackTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Title_Displayed() {
        val track = TestDataFactory.makeTrack()

        composeTestRule.setContent { NewTracks(track = track) }

        composeTestRule.onNodeWithText(track.title).assertIsDisplayed()
    }

    @Test
    fun assert_Artist_Displayed() {
        val track = TestDataFactory.makeTrack()

        composeTestRule.setContent { NewTracks(track = track) }

        composeTestRule.onNodeWithText(track.artist).assertIsDisplayed()
    }
}