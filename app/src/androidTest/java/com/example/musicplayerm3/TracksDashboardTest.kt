package com.example.musicplayerm3

import androidx.compose.ui.test.assert
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.hasTestTag
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onChildAt
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performScrollTo
import androidx.test.platform.app.InstrumentationRegistry
import com.example.musicplayerm3.data.MusicDashboardState
import com.example.musicplayerm3.data.Track
import com.example.musicplayerm3.ui.screens.track.TracksDashboard
import com.example.musicplayerm3.utils.Tags.TAG_TRACK
import com.example.musicplayerm3.utils.Tags.TAG_TRACKS_DASHBOARD
import com.example.musicplayerm3.utils.TestDataFactory
import dev.chrisbanes.snapper.ExperimentalSnapperApi
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

@ExperimentalSnapperApi class TracksDashboardTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    private fun getTestString(id: Int) =
        InstrumentationRegistry.getInstrumentation().targetContext.getString(id)

    @Test
    fun assert_Content_Area_Displayed() {
        composeTestRule.setContent {
            TracksDashboard(state = MusicDashboardState(), onTrackClicked = {})
        }

        composeTestRule.onNodeWithTag(TAG_TRACKS_DASHBOARD).assertIsDisplayed()
    }

    @Test
    fun assert_Recently_Played_Tracks_Header_Displayed() {
        composeTestRule.setContent {
            TracksDashboard(
                state = MusicDashboardState(
                    tracks = TestDataFactory.makeMixedContentList()
                ),
                onTrackClicked = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_TRACKS_DASHBOARD)
            .onChildAt(5)
            .assertTextEquals(getTestString(R.string.heading_recently_played))
    }

    @Test
    fun assert_Recently_Played_Tracks_Displayed() {
        val tracks = TestDataFactory.makeMixedContentList()
        val state = MusicDashboardState(tracks = tracks)
        val recentTracks = state.recentTracks()

        composeTestRule.setContent {
            TracksDashboard(state = state, onTrackClicked = {})
        }

        recentTracks.forEachIndexed { index, track ->
            composeTestRule.onNodeWithTag(TAG_TRACKS_DASHBOARD)
                .onChildAt(5 + index)
                .performScrollTo()
                .assert(hasTestTag(TAG_TRACK + track.id))
        }
    }

    @Test
    fun assert_Clicking_Recently_Played_Track_Plays_Selected_Track() {
        val tracks = TestDataFactory.makeMixedContentList()
        val state = MusicDashboardState(tracks = tracks)
        val trackToSelect = state.recentTracks().first()
        val trackListener: (track: Track) -> Unit = mock()

        composeTestRule.setContent {
            TracksDashboard(state = state, onTrackClicked = trackListener)
        }

        composeTestRule.onNodeWithTag(TAG_TRACKS_DASHBOARD)
            .onChildAt(5)
            .performClick()

        verify(trackListener).invoke(trackToSelect)
    }
}