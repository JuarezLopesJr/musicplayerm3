package com.example.musicplayerm3.data

data class MusicDashboardState(
    val status: Status = Status.LOADING,
    val tracks: List<Track>? = null,
    val nowPlaying: NowPlaying? = null,
    val searchTerm: String? = null,
    val searchResults: List<Track>? = null
) {
    fun newTracks() = tracks?.filter { it.isNew } ?: emptyList()

    fun featuredTracks() = tracks?.filter { it.isFeatured } ?: emptyList()

    fun libraryTracks() = tracks?.filter { !it.isNew && !it.isFeatured } ?: emptyList()

    fun recentTracks() = tracks?.filter { !it.isFeatured && !it.isNew } ?: emptyList()
}