package com.example.musicplayerm3.data

data class NowPlaying(
    val track: Track,
    val position: Long,
    val state: NowPlayingState
)