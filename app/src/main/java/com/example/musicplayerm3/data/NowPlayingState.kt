package com.example.musicplayerm3.data

enum class NowPlayingState {
    PLAYING, PAUSED, STOPPED
}