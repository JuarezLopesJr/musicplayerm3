package com.example.musicplayerm3.data

sealed class MusicDashboardEvent {
    object RefreshContent : MusicDashboardEvent()

    class Search(val searchTerm: String) : MusicDashboardEvent()

    object ClearSearchQuery : MusicDashboardEvent()

    class PlayTrack(val track: Track) : MusicDashboardEvent()

    object ToggleNowPlayingState : MusicDashboardEvent()

    class SeekTrack(val position: Float) : MusicDashboardEvent()

    object FastForwardNowPlaying : MusicDashboardEvent()

    object RewindNowPlaying : MusicDashboardEvent()
}