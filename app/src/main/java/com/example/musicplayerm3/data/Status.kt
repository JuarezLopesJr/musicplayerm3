package com.example.musicplayerm3.data

enum class Status {
    LOADING, SUCCESS, IDLE
}