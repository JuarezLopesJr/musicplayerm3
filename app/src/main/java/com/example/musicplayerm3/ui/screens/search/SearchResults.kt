package com.example.musicplayerm3.ui.screens.search

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.musicplayerm3.R
import com.example.musicplayerm3.data.Track
import com.example.musicplayerm3.ui.screens.track.TrackItem
import com.example.musicplayerm3.utils.Tags.TAG_SEARCH_RESULTS

@Composable
fun SearchResults(
    modifier: Modifier = Modifier,
    results: List<Track>?,
    onTrackClicked: (Track) -> Unit
) {
    if (results.isNullOrEmpty()) {
        Box(
            modifier = modifier,
            contentAlignment = Alignment.Center
        ) {
            Text(text = stringResource(id = R.string.message_search_results))
        }
    } else {
        LazyColumn(
            modifier = modifier.testTag(TAG_SEARCH_RESULTS),
            contentPadding = PaddingValues(vertical = 8.dp)
        ) {
            items(results) { track ->
                TrackItem(
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable(
                            onClickLabel = stringResource(
                                id = R.string.action_select_track,
                                track.title
                            )
                        ) { onTrackClicked(track) },
                    track = track
                )
            }
        }
    }
}