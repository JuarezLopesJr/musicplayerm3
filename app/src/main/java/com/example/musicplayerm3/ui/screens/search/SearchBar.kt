package com.example.musicplayerm3.ui.screens.search

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.musicplayerm3.R
import com.example.musicplayerm3.utils.Tags.TAG_SEARCH_BAR

@ExperimentalMaterial3Api
@Composable
fun SearchBar(
    modifier: Modifier = Modifier,
    query: String?,
    handleQuery: (String) -> Unit,
    clearQuery: () -> Unit
) {
    var inputHasFocus by remember { mutableStateOf(false) }
    val focusRequester = FocusRequester()
    val focusManager = LocalFocusManager.current
    val onSurfaceWithAlpha = MaterialTheme.colorScheme.onSurface.copy(alpha = 0.4f)

    Box(contentAlignment = Alignment.CenterStart) {
        TextField(
            modifier = modifier
                .testTag(TAG_SEARCH_BAR)
                .focusRequester(focusRequester)
                .onFocusChanged { inputHasFocus = it.hasFocus },
            colors = TextFieldDefaults.textFieldColors(
                textColor = MaterialTheme.colorScheme.onSurface,
                containerColor = MaterialTheme.colorScheme.surface,
                focusedIndicatorColor = MaterialTheme.colorScheme.onSurface.copy(
                    alpha = 0.6f
                )
            ),
            singleLine = true,
            value = query ?: "",
            onValueChange = { handleQuery(it) },
            trailingIcon = {
                if (query.isNullOrEmpty() && !inputHasFocus) {
                    Icon(
                        imageVector = Icons.Default.Search,
                        contentDescription = stringResource(R.string.cd_search)
                    )
                } else {
                    IconButton(
                        onClick = {
                            focusManager.clearFocus()
                            clearQuery()
                        },
                    ) {
                        Icon(
                            imageVector = Icons.Default.Clear,
                            contentDescription = stringResource(R.string.cd_clear_query)
                        )
                    }
                }
            }
        )

        if (!inputHasFocus && query.isNullOrEmpty()) {
            Text(
                modifier = Modifier.padding(start = 16.dp),
                color = onSurfaceWithAlpha,
                text = stringResource(R.string.hint_search)
            )
        }
    }
}