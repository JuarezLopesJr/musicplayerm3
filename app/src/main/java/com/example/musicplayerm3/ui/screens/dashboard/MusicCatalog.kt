package com.example.musicplayerm3.ui.screens.dashboard

import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.musicplayerm3.viewmodel.MusicViewModel
import dev.chrisbanes.snapper.ExperimentalSnapperApi

@ExperimentalSnapperApi
@ExperimentalMaterialApi
@ExperimentalMaterial3Api
@ExperimentalLifecycleComposeApi
@Composable
fun MusicCatalog() {
    val viewModel = viewModel<MusicViewModel>()
    val state by viewModel.uiState.collectAsStateWithLifecycle()

    Dashboard(state = state, handleEvent = viewModel::handleEvent)
}