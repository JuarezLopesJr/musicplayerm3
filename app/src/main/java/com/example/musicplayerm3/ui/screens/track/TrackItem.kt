package com.example.musicplayerm3.ui.screens.track

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.musicplayerm3.data.Track
import com.example.musicplayerm3.utils.CoverArt

@Composable
fun TrackItem(
    modifier: Modifier = Modifier,
    track: Track
) {
    Surface(
        modifier = modifier.semantics(mergeDescendants = true) {},
        shape = RoundedCornerShape(8.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp, vertical = 8.dp),
        ) {
            CoverArt(modifier = Modifier.size(40.dp), track = track)

            Spacer(modifier = Modifier.width(12.dp))

            Column {
                Text(text = track.title, fontSize = 16.sp)
                Text(text = track.artist, fontSize = 12.sp)
            }
        }
    }
}