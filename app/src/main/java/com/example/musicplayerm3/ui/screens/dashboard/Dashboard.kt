package com.example.musicplayerm3.ui.screens.dashboard

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.BackdropScaffold
import androidx.compose.material.BackdropValue
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.rememberBackdropScaffoldState
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.unit.dp
import com.example.musicplayerm3.data.MusicDashboardEvent
import com.example.musicplayerm3.data.MusicDashboardState
import com.example.musicplayerm3.ui.screens.player.MusicPlayer
import com.example.musicplayerm3.ui.screens.search.SearchBar
import com.example.musicplayerm3.ui.screens.track.TracksDashboard
import com.example.musicplayerm3.utils.Tags.TAG_DASHBOARD
import com.example.musicplayerm3.utils.Tags.TAG_SEARCH_BAR
import dev.chrisbanes.snapper.ExperimentalSnapperApi

@ExperimentalSnapperApi
@ExperimentalMaterial3Api
@ExperimentalMaterialApi
@Composable
fun Dashboard(
    modifier: Modifier = Modifier,
    state: MusicDashboardState,
    handleEvent: (MusicDashboardEvent) -> Unit
) {
    LaunchedEffect(key1 = Unit) {
        handleEvent(MusicDashboardEvent.RefreshContent)
    }

    val scaffoldState = rememberBackdropScaffoldState(
        initialValue = BackdropValue.Revealed
    )

    BackdropScaffold(
        modifier = modifier.testTag(TAG_DASHBOARD),
        headerHeight = 64.dp,
        peekHeight = 0.dp,
        gesturesEnabled = false,
        backLayerBackgroundColor = Color.Black,
        scaffoldState = scaffoldState,
        frontLayerShape = RectangleShape,
        appBar = {
            SearchBar(
                modifier = Modifier
                    .testTag(TAG_SEARCH_BAR)
                    .fillMaxWidth(),
                query = state.searchTerm,
                handleQuery = {
                    handleEvent(MusicDashboardEvent.Search(it))
                },
                clearQuery = {
                    handleEvent(MusicDashboardEvent.ClearSearchQuery)
                }
            )
        },
        backLayerContent = {
            TracksDashboard(
                modifier = Modifier.fillMaxSize(),
                state = state,
                onTrackClicked = {
                    handleEvent(MusicDashboardEvent.PlayTrack(it))
                }
            )
        },
        frontLayerContent = {
            MusicPlayer(
                modifier = Modifier.fillMaxSize(),
                state = state,
                scaffoldState = scaffoldState,
                handleEvent = handleEvent
            )
        },
        frontLayerScrimColor = Color.Unspecified
    )
}