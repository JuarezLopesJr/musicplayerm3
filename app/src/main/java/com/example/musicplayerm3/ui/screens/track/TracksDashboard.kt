package com.example.musicplayerm3.ui.screens.track

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.unit.dp
import com.example.musicplayerm3.data.MusicDashboardState
import com.example.musicplayerm3.data.Track
import com.example.musicplayerm3.ui.screens.search.SearchResults
import com.example.musicplayerm3.utils.Tags.TAG_TRACKS_DASHBOARD
import com.example.musicplayerm3.utils.scopeFeaturedTracks
import com.example.musicplayerm3.utils.scopeNewTracks
import com.example.musicplayerm3.utils.scopeRecentTracks
import dev.chrisbanes.snapper.ExperimentalSnapperApi

@ExperimentalSnapperApi
@Composable
fun TracksDashboard(
    modifier: Modifier = Modifier,
    state: MusicDashboardState,
    onTrackClicked: (Track) -> Unit
) {
    if (state.searchTerm.isNullOrEmpty()) {
        LazyColumn(
            modifier = modifier.testTag(TAG_TRACKS_DASHBOARD),
            contentPadding = PaddingValues(
                top = 12.dp,
                bottom = 8.dp,
            ),
        ) {
            scopeFeaturedTracks(
                tracks = state.featuredTracks(),
                onTrackClicked = onTrackClicked
            )

            scopeNewTracks(
                tracks = state.newTracks(),
                onTrackClicked = onTrackClicked
            )

            scopeRecentTracks(
                tracks = state.recentTracks(),
                onTrackClicked = onTrackClicked
            )
        }
    } else {
        SearchResults(
            modifier = Modifier.fillMaxWidth(),
            results = state.searchResults,
            onTrackClicked = onTrackClicked
        )
    }
}