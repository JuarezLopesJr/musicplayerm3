package com.example.musicplayerm3.ui.screens.player

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Cancel
import androidx.compose.material.icons.filled.FastForward
import androidx.compose.material.icons.filled.FastRewind
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.musicplayerm3.R
import com.example.musicplayerm3.data.NowPlaying
import com.example.musicplayerm3.utils.CoverArt
import com.example.musicplayerm3.utils.Tags.TAG_DISMISS_PLAYER
import com.example.musicplayerm3.utils.descriptionForPlayingState
import com.example.musicplayerm3.utils.iconForPlayingState
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

@Composable
fun Player(
    modifier: Modifier = Modifier,
    nowPlaying: NowPlaying,
    toggleNowPlayingState: () -> Unit,
    rewindTrack: () -> Unit,
    fastForwardTrack: () -> Unit,
    onSeekChanged: (Float) -> Unit,
    onClose: () -> Unit
) {
    val dateFormat = remember {
        SimpleDateFormat("mm:ss", Locale.getDefault())
    }

    Column(
        modifier = modifier.padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        IconButton(
            modifier = Modifier
                .testTag(TAG_DISMISS_PLAYER)
                .align(Alignment.End),
            onClick = { onClose() }
        ) {
            Icon(
                imageVector = Icons.Default.Cancel,
                contentDescription = stringResource(id = R.string.cd_close_now_playing)
            )
        }

        CoverArt(
            track = nowPlaying.track,
            modifier = Modifier.size(240.dp)
        )

        Spacer(modifier = Modifier.height(16.dp))

        Column(
            modifier = Modifier.semantics(mergeDescendants = true) {},
        ) {
            Text(text = nowPlaying.track.title, fontSize = 22.sp)

            Text(
                text = nowPlaying.track.artist,
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold
            )
        }

        Spacer(modifier = Modifier.height(36.dp))

        PlayerSeekBar(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 32.dp),
            nowPlaying = nowPlaying,
            canSeekTrack = true,
            onSeekChanged = onSeekChanged
        )

        Spacer(modifier = Modifier.height(8.dp))

        Row(
            modifier = Modifier.padding(horizontal = 32.dp),
        ) {
            Text(text = dateFormat.format(Date(nowPlaying.position * 1000L)))
            /* forcing space between timestamps */
            Spacer(modifier = Modifier.weight(1f))

            Text(text = dateFormat.format(Date(nowPlaying.track.length * 1000L)))
        }

        Row(horizontalArrangement = Arrangement.SpaceEvenly) {
            IconButton(onClick = { rewindTrack() }) {
                Icon(
                    imageVector = Icons.Default.FastRewind,
                    contentDescription = stringResource(id = R.string.cd_rewind)
                )
            }

            IconButton(onClick = { toggleNowPlayingState() }) {
                Icon(
                    imageVector = iconForPlayingState(nowPlaying.state),
                    contentDescription =
                    stringResource(descriptionForPlayingState(nowPlaying.state))
                )
            }

            IconButton(onClick = { fastForwardTrack() }) {
                Icon(
                    imageVector = Icons.Default.FastForward,
                    contentDescription = stringResource(id = R.string.cd_fast_forward)
                )
            }

        }
    }
}