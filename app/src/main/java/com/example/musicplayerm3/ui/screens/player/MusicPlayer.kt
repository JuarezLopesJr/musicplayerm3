package com.example.musicplayerm3.ui.screens.player

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.snap
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.BackdropScaffoldState
import androidx.compose.material.BackdropValue
import androidx.compose.material.Divider
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.musicplayerm3.R
import com.example.musicplayerm3.data.MusicDashboardEvent
import com.example.musicplayerm3.data.MusicDashboardState
import com.example.musicplayerm3.utils.Tags.TAG_PLAYER
import com.example.musicplayerm3.utils.Tags.TAG_PLAYER_BAR
import kotlinx.coroutines.launch

@ExperimentalMaterialApi
@Composable
fun MusicPlayer(
    modifier: Modifier = Modifier,
    state: MusicDashboardState,
    scaffoldState: BackdropScaffoldState,
    handleEvent: (MusicDashboardEvent) -> Unit
) {
    val scope = rememberCoroutineScope()

    AnimatedVisibility(
        modifier = modifier,
        visible = scaffoldState.targetValue == BackdropValue.Concealed,
        enter = fadeIn(),
        exit = fadeOut()
    ) {
        state.nowPlaying?.let { nowPlaying ->
            Player(
                modifier = Modifier
                    .testTag(TAG_PLAYER)
                    .fillMaxSize(),
                nowPlaying = nowPlaying,
                toggleNowPlayingState = { handleEvent(MusicDashboardEvent.ToggleNowPlayingState) },
                rewindTrack = { handleEvent(MusicDashboardEvent.RewindNowPlaying) },
                fastForwardTrack = { handleEvent(MusicDashboardEvent.FastForwardNowPlaying) },
                onSeekChanged = {
                    handleEvent(MusicDashboardEvent.SeekTrack(it))
                },
                onClose = {
                    scope.launch {
                        scaffoldState.reveal()
                    }
                }
            )
        }
    }

    AnimatedVisibility(
        modifier = Modifier.wrapContentHeight(),
        visible = scaffoldState.targetValue == BackdropValue.Revealed,
        enter = fadeIn(),
        exit = fadeOut(animationSpec = snap())
    ) {
        Column {

            Divider(
                modifier = Modifier.height(2.dp),
                color = MaterialTheme.colorScheme.onSurface.copy(alpha = 0.4f)
            )

            PlayerBar(
                modifier = modifier
                    .testTag(TAG_PLAYER_BAR)
                    .fillMaxWidth()
                    .clickable(onClickLabel = stringResource(id = R.string.action_show_player)) {
                        scope.launch {
                            scaffoldState.conceal()
                        }
                    },
                nowPlaying = state.nowPlaying,
                toggleNowPlayingState = {
                    handleEvent(MusicDashboardEvent.ToggleNowPlayingState)
                }
            )
        }
    }
}