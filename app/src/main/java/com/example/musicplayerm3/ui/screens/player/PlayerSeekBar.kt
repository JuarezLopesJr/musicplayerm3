package com.example.musicplayerm3.ui.screens.player

import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.Slider
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import com.example.musicplayerm3.data.NowPlaying
import com.example.musicplayerm3.utils.Tags.TAG_LINEAR_BAR
import com.example.musicplayerm3.utils.Tags.TAG_SEEK_BAR

@Composable
fun PlayerSeekBar(
    modifier: Modifier = Modifier,
    nowPlaying: NowPlaying,
    canSeekTrack: Boolean,
    onSeekChanged: ((Float) -> Unit)? = null
) {
    val currentPosition = nowPlaying.position.toFloat() / nowPlaying.track.length.toFloat()

    if (canSeekTrack) {
        Slider(
            modifier = modifier.testTag(TAG_SEEK_BAR),
            value = currentPosition,
            onValueChange = {
                onSeekChanged?.invoke(nowPlaying.track.length * it)
            }
        )
    } else {
        LinearProgressIndicator(
            modifier = modifier.testTag(TAG_LINEAR_BAR),
            progress = currentPosition
        )
    }
}