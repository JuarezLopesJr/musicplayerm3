package com.example.musicplayerm3.ui.screens.player

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.musicplayerm3.data.NowPlaying
import com.example.musicplayerm3.utils.CoverArt
import com.example.musicplayerm3.utils.Tags.TAG_PLAY_PAUSE
import com.example.musicplayerm3.utils.Tags.TAG_TRACK_ARTIST
import com.example.musicplayerm3.utils.Tags.TAG_TRACK_TITLE
import com.example.musicplayerm3.utils.descriptionForPlayingState
import com.example.musicplayerm3.utils.iconForPlayingState

@Composable
fun PlayerBar(
    modifier: Modifier = Modifier,
    nowPlaying: NowPlaying?,
    toggleNowPlayingState: () -> Unit
) {
    nowPlaying?.let {
        Row(
            modifier = modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            CoverArt(
                modifier = Modifier.size(23.dp),
                track = it.track
            )

            Spacer(modifier = Modifier.width(16.dp))

            Column(modifier = Modifier
                .semantics(mergeDescendants = true) {}
                .padding(horizontal = 8.dp, vertical = 4.dp)
                .weight(1f)
            ) {
                Text(
                    modifier = Modifier.testTag(TAG_TRACK_TITLE),
                    text = it.track.title,
                    fontWeight = FontWeight.Bold,
                    fontSize = 12.sp
                )

                Text(
                    modifier = Modifier.testTag(TAG_TRACK_ARTIST),
                    text = it.track.artist,
                    fontSize = 10.sp
                )

                PlayerSeekBar(
                    modifier = Modifier.fillMaxWidth(),
                    nowPlaying = nowPlaying,
                    canSeekTrack = false
                )
            }

            IconButton(
                modifier = Modifier.testTag(TAG_PLAY_PAUSE),
                onClick = toggleNowPlayingState
            ) {
                Icon(
                    imageVector = iconForPlayingState(it.state),
                    contentDescription = stringResource(descriptionForPlayingState(it.state))
                )
            }
        }
    }
}