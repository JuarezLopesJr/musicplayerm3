package com.example.musicplayerm3.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.musicplayerm3.data.MusicDashboardEvent
import com.example.musicplayerm3.data.MusicDashboardState
import com.example.musicplayerm3.data.NowPlayingState
import com.example.musicplayerm3.data.Status
import com.example.musicplayerm3.utils.ContentFactory
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch

class MusicViewModel : ViewModel() {
    private val _uiState = MutableStateFlow(MusicDashboardState())
    private var nowPlayingFlow: Job? = null

    val uiState = _uiState.asStateFlow()

    private fun loadContent() {
        val data = ContentFactory.makeContentList()

        _uiState.update {
            it.copy(
                status = Status.SUCCESS,
                tracks = data,
                nowPlaying = ContentFactory.makeNowPlaying(data[0])
            )
        }
    }

    private fun searchContent(searchTerm: String) {
        val searchResults = _uiState.value.tracks?.filter {
            it.title.contains(searchTerm, ignoreCase = true) ||
                    it.artist.contains(searchTerm, ignoreCase = true)
        }

        _uiState.update {
            it.copy(
                searchTerm = searchTerm,
                searchResults = searchResults
            )
        }
    }

    /* handle playback simulation, before start playing the newly selected track, first i need
      to cancell the previous track */
    private fun playMusic() {
        /* stopping current track if started previously */
        nowPlayingFlow?.cancel()

        _uiState.update {
            it.copy(
                nowPlaying = _uiState.value.nowPlaying!!.copy(
                    state = NowPlayingState.PLAYING
                )
            )
        }

        /* simulating playback */
        nowPlayingFlow = viewModelScope.launch {
            while (isActive) {
                val trackLength = _uiState.value.nowPlaying!!.track.length
                val newPosition = _uiState.value.nowPlaying!!.position + 1

                /* checking if the track time is over */
                if (newPosition >= trackLength) {
                    _uiState.update {
                        it.copy(
                            nowPlaying = _uiState.value.nowPlaying!!.copy(
                                state = NowPlayingState.STOPPED,
                                position = 0
                            )
                        )
                    }

                    cancel()
                } else {
                    _uiState.update {
                        it.copy(
                            nowPlaying = _uiState.value.nowPlaying!!.copy(
                                state = NowPlayingState.PLAYING,
                                position = newPosition
                            )
                        )
                    }
                }
                delay(1000)
            }
        }
    }

    private fun pauseMusic() {
        nowPlayingFlow?.cancel()

        _uiState.update {
            it.copy(
                nowPlaying = _uiState.value.nowPlaying!!.copy(
                    state = NowPlayingState.PAUSED
                )
            )
        }
    }

    private fun toggleNowPlayingState() {
        if (_uiState.value.nowPlaying?.state == NowPlayingState.PLAYING) {
            pauseMusic()
        } else {
            playMusic()
        }
    }

    private fun fastFowardMusic() {
        val newPosition = _uiState.value.nowPlaying!!.position + 10
        val trackLength = _uiState.value.nowPlaying!!.track.length

        _uiState.update {
            it.copy(
                nowPlaying = _uiState.value.nowPlaying!!.copy(
                    position = if (newPosition > trackLength) trackLength else newPosition
                )
            )
        }
    }

    private fun rewindMusic() {
        val newPosition = _uiState.value.nowPlaying!!.position - 10

        _uiState.update {
            it.copy(
                nowPlaying = _uiState.value.nowPlaying!!.copy(
                    position = if (newPosition < 0) 0 else newPosition
                )
            )
        }
    }

    fun handleEvent(event: MusicDashboardEvent) {
        when (event) {
            MusicDashboardEvent.RefreshContent -> loadContent()

            is MusicDashboardEvent.Search -> searchContent(searchTerm = event.searchTerm)

            MusicDashboardEvent.ClearSearchQuery -> {
                _uiState.update {
                    it.copy(
                        searchTerm = null
                    )
                }
            }

            is MusicDashboardEvent.PlayTrack -> {
                _uiState.update {
                    it.copy(
                        nowPlaying = ContentFactory.makeNowPlaying(event.track)
                    )
                }
                playMusic()
            }

            MusicDashboardEvent.ToggleNowPlayingState -> toggleNowPlayingState()

            is MusicDashboardEvent.SeekTrack -> {
                _uiState.update {
                    it.copy(
                        nowPlaying = _uiState.value.nowPlaying!!.copy(
                            position = event.position.toLong()
                        )
                    )
                }
            }

            MusicDashboardEvent.FastForwardNowPlaying -> fastFowardMusic()

            MusicDashboardEvent.RewindNowPlaying -> rewindMusic()
        }
    }

    /* preventing viewmodel leaks */
    override fun onCleared() {
        nowPlayingFlow?.cancel()
        super.onCleared()
    }
}