package com.example.musicplayerm3.utils

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Pause
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.musicplayerm3.R
import com.example.musicplayerm3.data.NowPlayingState
import com.example.musicplayerm3.data.Track
import com.example.musicplayerm3.ui.screens.track.TrackItem
import com.example.musicplayerm3.utils.Tags.TAG_FEATURED_TRACKS
import com.example.musicplayerm3.utils.Tags.TAG_NEW_TRACKS
import com.example.musicplayerm3.utils.Tags.TAG_TRACK
import com.google.accompanist.flowlayout.FlowMainAxisAlignment
import com.google.accompanist.flowlayout.FlowRow
import com.google.accompanist.flowlayout.SizeMode
import dev.chrisbanes.snapper.ExperimentalSnapperApi
import dev.chrisbanes.snapper.rememberSnapperFlingBehavior

@Composable
fun CoverArt(
    modifier: Modifier = Modifier,
    track: Track
) {
    Box(
        modifier = modifier
            .background(
                brush = Brush.verticalGradient(
                    colors = listOf(
                        MaterialTheme.colorScheme.primary,
                        track.cover
                    )
                ),
                shape = RoundedCornerShape(8.dp)
            )
    )
}

@Composable
fun NewTracks(
    modifier: Modifier = Modifier,
    track: Track
) {
    Column(
        modifier = modifier
            .semantics(mergeDescendants = true) {}
            .padding(8.dp)
            .widthIn(max = 120.dp),
    ) {
        CoverArt(
            modifier = Modifier
                .fillMaxWidth()
                .height(120.dp),
            track = track
        )

        Spacer(modifier = Modifier.height(8.dp))

        Text(
            text = track.title,
            fontWeight = FontWeight.Bold,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            fontSize = 16.sp
        )

        Text(
            text = track.artist,
            fontSize = 12.sp
        )
    }
}

/* this snapper lib ensures that LazyRow content is always positioned at the start of the
   visible area */
@ExperimentalSnapperApi
@Composable
fun NewTracksRow(
    modifier: Modifier = Modifier,
    tracks: List<Track>?,
    onTrackClicked: (Track) -> Unit
) {
    if (!tracks.isNullOrEmpty()) {
        val state = rememberLazyListState()

        LazyRow(
            modifier = modifier.testTag(TAG_NEW_TRACKS),
            state = state,
            flingBehavior = rememberSnapperFlingBehavior(lazyListState = state),
            contentPadding = PaddingValues(8.dp)
        ) {
            items(tracks) { track ->
                NewTracks(
                    modifier = Modifier
                        .testTag(TAG_TRACK + track.id)
                        .fillMaxWidth()
                        .clickable(
                            onClickLabel = stringResource(
                                id = R.string.cd_play_track,
                                track.title,
                                track.artist
                            )
                        ) {
                            onTrackClicked(track)
                        },
                    track = track
                )
            }
        }
    }
}

@Composable
fun FeaturedTracksGrid(
    modifier: Modifier = Modifier,
    tracks: List<Track>?,
    onTrackClicked: (Track) -> Unit
) {
    if (!tracks.isNullOrEmpty()) {
        val itemSize = (LocalConfiguration.current.screenWidthDp.dp / 2)

        FlowRow(
            modifier = modifier.testTag(TAG_FEATURED_TRACKS),
            mainAxisSize = SizeMode.Expand,
            mainAxisAlignment = FlowMainAxisAlignment.SpaceBetween
        ) {
            tracks.forEach { track ->
                FeaturedTrack(
                    modifier = Modifier
                        .testTag(TAG_TRACK + track.id)
                        .width(itemSize)
                        .clickable { onTrackClicked(track) },
                    track = track
                )
            }
        }
    }
}

@Composable
fun FeaturedTrack(
    modifier: Modifier = Modifier,
    track: Track
) {
    Surface(
        modifier = modifier,
        shape = RoundedCornerShape(8.dp),
        shadowElevation = 4.dp
    ) {
        Row(
            modifier = Modifier
                .semantics(mergeDescendants = true) {}
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            CoverArt(
                modifier = Modifier.size(52.dp),
                track = track
            )

            Text(
                modifier = Modifier.padding(8.dp),
                text = track.title,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                fontSize = 14.sp,
                fontWeight = FontWeight.Bold
            )
        }
    }
}

/* extension functions to break responsibility from TracksDashboard */
fun LazyListScope.scopeRecentTracks(
    tracks: List<Track>,
    onTrackClicked: (Track) -> Unit
) {
    item {
        Text(
            modifier = Modifier.padding(start = 16.dp, bottom = 8.dp),
            text = stringResource(id = R.string.heading_recently_played),
            fontSize = 18.sp,
            fontWeight = FontWeight.Bold
        )
    }

    items(tracks, key = { it.id }) { track ->
        TrackItem(
            modifier = Modifier
                .testTag(TAG_TRACK + track.id)
                .fillMaxWidth()
                .clickable(
                    onClickLabel = stringResource(
                        id = R.string.cd_play_track,
                        track.title,
                        track.artist
                    )
                ) {
                    onTrackClicked(track)
                },
            track = track
        )
    }
}

@ExperimentalSnapperApi
fun LazyListScope.scopeNewTracks(
    tracks: List<Track>?,
    onTrackClicked: (Track) -> Unit
) {
    item {
        Text(
            modifier = Modifier.padding(start = 16.dp, bottom = 8.dp),
            text = stringResource(id = R.string.heading_new),
            fontSize = 18.sp,
            fontWeight = FontWeight.Bold
        )

        Spacer(modifier = Modifier.height(8.dp))

        NewTracksRow(tracks = tracks, onTrackClicked = onTrackClicked)
    }
}

fun LazyListScope.scopeFeaturedTracks(
    tracks: List<Track>?,
    onTrackClicked: (Track) -> Unit
) {
    item {
        Text(
            modifier = Modifier.padding(start = 16.dp),
            text = stringResource(id = R.string.heading_featured),
            fontSize = 18.sp,
            fontWeight = FontWeight.Bold
        )

        FeaturedTracksGrid(
            modifier = Modifier.fillMaxWidth(),
            tracks = tracks,
            onTrackClicked = onTrackClicked
        )
    }
}

fun iconForPlayingState(state: NowPlayingState): ImageVector {
    return if (state == NowPlayingState.PLAYING) {
        Icons.Default.Pause
    } else {
        Icons.Default.PlayArrow
    }
}

fun descriptionForPlayingState(state: NowPlayingState): Int {
    return if (state == NowPlayingState.PLAYING) {
        R.string.cd_pause
    } else {
        R.string.cd_play
    }
}

object Tags {
    const val TAG_DASHBOARD = "tag_dashboard"
    const val TAG_SEARCH_BAR = "tag_search_bar"
    const val TAG_PLAYER_BAR = "tag_player_bar"
    const val TAG_PLAYER = "tag_player"
    const val TAG_DISMISS_PLAYER = "tag_dismiss_player"
    const val TAG_TRACKS_DASHBOARD = "tag_tracks_dashboard"
    const val TAG_TRACK = "tag_track_"
    const val TAG_FEATURED_TRACKS = "tag_featured_tracks"
    const val TAG_NEW_TRACKS = "tag_new_tracks"
    const val TAG_SEARCH_RESULTS = "tag_search_results"
    const val TAG_SEEK_BAR = "tag_seek_bar"
    const val TAG_LINEAR_BAR = "tag_linear_bar"
    const val TAG_TRACK_TITLE = "tag_track_title"
    const val TAG_TRACK_ARTIST = "tag_track_artist"
    const val TAG_PLAY_PAUSE = "tag_play_pause"
}