package com.example.musicplayerm3.utils

import androidx.compose.ui.graphics.Color
import com.example.musicplayerm3.data.NowPlaying
import com.example.musicplayerm3.data.NowPlayingState
import com.example.musicplayerm3.data.Track
import java.util.UUID
import kotlin.random.Random.Default.nextLong

object TestDataFactory {
    private fun randomString() = UUID.randomUUID().toString().substring(0, 10)

    fun makeTrack(
        isFeatured: Boolean = Math.random() < 0.5,
        isNew: Boolean = Math.random() < 0.5
    ): Track {
        return Track(
            id = randomString(),
            title = randomString(),
            artist = randomString(),
            cover = Color.Green,
            length = nextLong(500),
            isNew = isNew,
            isFeatured = isFeatured
        )
    }

    fun makeNowPlaying(
        track: Track = makeTrack(),
        state: NowPlayingState = NowPlayingState.PAUSED,
        progress: Long = track.length / 3
    ): NowPlaying {
        return NowPlaying(
            track = track,
            position = progress,
            state = state
        )
    }

    fun makeContentList(count: Int): List<Track> {
        return (0..count).map { makeTrack() }
    }

    fun makeMixedContentList(): List<Track> {
        return listOf(
            makeTrack(isFeatured = false, isNew = false),
            makeTrack(isFeatured = true, isNew = true),
            makeTrack(isFeatured = true, isNew = false),
            makeTrack(isFeatured = false, isNew = true),
            makeTrack(isFeatured = true, isNew = true),
            makeTrack(isFeatured = false, isNew = false),
            makeTrack(isFeatured = false, isNew = true),
            makeTrack(isFeatured = true, isNew = false)
        )
    }
}